---
title: MinIO
description: Multi-Cloud Object Storage 多云对象存储
date: 2023-01-01
slug: minio
image: img/minio.jpg
tags: 
    - object storage
categories:
    - devops
---

### 特点
1. 开源，安装及其简单快捷，开箱即用
2. 提供亚马逊云S3兼容性API并支持S3所有特性
3. 可以在任何地方构建，包括公私有云、裸基础设施、协调环境和边缘基础设施
4. 对标目前市面上流行的公有云服务, 譬如S3、COS、OSS等

### Try on MacOS

> Suitable on AMD64 & MacOS

1. 安装
    ```bash
    curl -O https://dl.min.io/server/minio/release/darwin-amd64/minio
    chmod +x ./minio
    sudo mv ./minio /usr/local/bin/
    ```

2. 启动
    ```bash
    export MINIO_CONFIG_ENV_FILE=/etc/default/minio #配置文件及端口
    minio server [/Users/[Your Name]/MinIO:自定义存储目录] --console-address :8787
    ``` 

3. 访问
    ```bash
    http://127.0.0.1:8787
    default login: minioadmin / minioadmin 
    ```

![Home](home.jpg) 

> 支持Kubernetes、Linux、Windows等系统或平台

### 其他
[官方安装直通车](https://min.io/docs/minio/linux/operations/install-deploy-manage/deploy-minio-multi-node-multi-drive.html?ref=docs-redirect)

