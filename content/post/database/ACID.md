---
title: "ACID"
description: 数据库事务的四大特性
date: 2023-03-14
slug: acid
image: img/mysql.png
tags: 
    - ACID
categories:
    - backend
---

Ref. [MySQL 5.6参考手册](https://docs.oracle.com/cd/E17952_01/mysql-5.6-en/mysql-acid.html)

## 数据库事务的四大特性
`原子性（Atomicity）、一致性（Consistency）、隔离性（Isolation）、持久性（Durability）`

ACID 模型是一组数据库设计原则，强调对业务数据和关键任务应用程序很重要的**可靠性**方面。
MySQL 包括诸如InnoDB严格遵守 ACID 模型的存储引擎，因此数据不会损坏，结果也不会因软件崩溃和硬件故障等异常情况而失真。

### A-原子性（Atomicity）
> 原子性是指事务是一个不可分割的工作单位，事务中的操作要么全部成功，要么全部失败。比如在同一个事务中的SQL语句，要么全部执行成功，要么全部执行失败。
> 关键语法： AutoCommit、COMMIT、ROLLBACK

```sql
begin transaction;
    update account set pass = 'xxx' where id=1;
    update order set money = 100 where order_id = 1001;
commit transaction;
```

### C-一致性（Consistency）
> 简单理解: 事务按预期执行后，数据达到预期状态。举例说明，你从银行卡转100元到老婆账户，你银行账号减少100，而你老婆账户增加100，这就是预期一致，这里应该更多的是强调数据库内部防崩溃机制。

### I-隔离性（Isolation）
> 事务的隔离性是多个用户并发访问数据库时，数据库为每一个用户开启的事务，不能被其他事务的操作数据所干扰，多个并发事务之间要相互隔离。

#### 分四种隔离级别
- Read committed
> 业务的中间状态对外不可见。很容易找到的一个例子是：甲向乙转账，银行先向乙的账户转入 100 元，再从甲的账户扣除 100 元。在银行扣款前，甲、乙的账户里都有 100 元。如果这一中间状态对外可见——甲和乙就有机会都花掉这 100 元，让银行扣款失败，而且没有办法追回。

- Repeatable reads / Serializable
> 这两个级别都在限制事务执行时，其他事务提交的更新是否可见。但是程度不同。Repeatable reads 只限制行，而 Serializable 限制整个数据集。
> 举个简单的例子来理解：生成业务报表，过程先查询订单明细数据，再查询汇总数据。如果业务在处理完明细数据后，又读到了其他业务修改的订单金额 (这会违反 Repeatable reads)，或者又读到了新的订单 (这会违反 Serializable)，那么最终得到的汇总数据就会与明细数据不一致，这对于商业敏感的报表是无法接受的。

- Read uncommitted
> 业务的中间状态对外可见，不需要事务隔离。这样看似危险，但不一定会产生业务问题。例如，在一个真实的转账里：银行会先从甲的账户扣除 100 元，再向乙的账户转入 100 元。这样，在乙账户到账前，甲的账户已经扣掉了 100 元。显然，即使这个中间状态对外可见，甲、乙也没有任何机会造成银行转账失败。

### D-持久性（Durability）
> 一个事务提交成功后，它对数据库中数据的改变就是永久性的，接下来即使数据库发生故障也不应该对其有任何影响（业务数据不会丢失）。


