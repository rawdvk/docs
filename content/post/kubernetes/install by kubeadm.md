---
title: "Cluster-K8s: Install by kubeadm"
description: 官方Kubeadm安装K8s笔记
date: 2023-01-01
slug: install_by_kubeadm
image: img/k8s.png
tags: 
    - kuberenetes/k8s
    - kubeadm
categories:
    - devops 
---

### 一、安装环境及工具
```text
使用 MacOS 演示
需提前安装 VMware 虚拟机 
下载ISO镜像: Ubuntu 18.04 [https://mirrors.163.com]
Kubernetes: v1.26.0
```

### 二、服务器规划
| Name    | Env  | IP             | Role   |
| ------- | ---- | -------------- | -------|
| kube-01 | 2C4G | 172.16.239.132 | Master |
| kube-02 | 2C4G | 172.16.239.133 | Node   |
| kube-03 | 2C4G | 172.16.239.134 | Node   |

### 三、服务器安装系统及网络配置
#### 3.1 系统安装
> 这里选择了Ubuntu 18.04，使用VMware虚拟机依次安装3台。

##### 3.1.1 关闭防火墙等
```bash
$ vi /etc/sysconfig/selinux -> SELINUX=disabled 
$ sethenforce 0
$ systemctl disable firewalld
```

##### 3.1.2 关闭Swap分区
```bash
$ swapoff -a #关闭swap
$ vi /etc/fstab #删除最后一行, swap....
$ free -m #查看swap关闭情况
$ sudo mount -a
```

#### 3.2 网络配置
> 第一种方式：在安装过程中，可直接配置网络，除了IP按规划表中设置外，网关需要在宿主机上检索下:
```bash
#NAT网络模式
$ cat /Library/Preferences/VMware\ Fusion/vmnet8/nat.conf | grep gateway -A 2`

#这里得到网关地址 `172.16.239.2`
```

> 第二种方式：进入服务器设置
```bash
#注意ubuntu18版本的网络配置文件位置
$ sudo vi /etc/netplan/00-installer-config.yaml

#改完应用命令
$ sudo netplan apply
```

```yaml
# 修改文件(/etc/netplan/00-installer-config.yaml)内容如下:
# This is the network config written by 'subiquity'
	network:
	  ethernets:
	    ens33:
	      addresses:
	      - 172.16.239.132/24
	      gateway4: 172.16.239.2
	      nameservers:
	        addresses:
	        - 172.16.239.2
	        - 114.114.114.114
	        - 8.8.4.4
	        search: []
	  version: 2
```

> 配置完后，会发现网络不通(systemd-resolved服务导致的dns解析问题)，需执行以下操作:
```bash
$ rm -rf /etc/resolv.cnf
$ ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf
$ sudo netplan apply && systemctl restart systemd-resolved.service
```

### 四、开始安装Kubernetes

#### 4.1 install containerd & runc
```bash
# 3台执行相同操作
## containerd
$ wget https://github.com/containerd/containerd/releases/download/v1.6.19/containerd-1.6.19-linux-amd64.tar.gz
$ tar Cxzvf /usr/local containerd-1.6.19-linux-amd64.tar.gz
$ vi /lib/systemd/system/containerd.service
$ systemctl daemon-reload
$ systemctl enable --now containerd.service
	
## runc
$ wget https://github.com/opencontainers/runc/releases/download/v1.1.4/runc.amd64
$ install -m 755 runc.amd64 /usr/local/sbin/runc
## CNI plugins
$ wget https://github.com/containernetworking/plugins/releases/download/v1.2.0/cni-plugins-linux-amd64-v1.2.0.tgz
$ mkdir -p /opt/cni/bin
$ tar Cxzvf /opt/cni/bin cni-plugins-linux-amd64-v1.2.0.tgz

```

#### 4.2 install kubeadm & kubelet & kubectl
```bash
# 3台执行相同操作
$ apt-get update
$ apt-get install -y apt-transport-https ca-certificates curl
$ mkdir -p /etc/apt/keyrings
$ curl -fsSLo /etc/apt/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
$ echo "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
$ apt-get update
$ apt-get install -y kubelet kubeadm kubectl
$ apt-mark hold kubelet kubeadm kubectl
```

#### 4.3 configuration
```bash
# master
## 生成默认的kubeadm.conf文件
$ kubeadm config print init-defaults > kubeadm.conf #修改networking.serviceSubnet: 10.188.0.0/12
## 初始化安装
$ kubeadm init --config kubeadm.conf
## 创建.kube配置文件
$ mkdir -p $HOME/.kube
$ sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
$ sudo chown $(id -u):$(id -g) $HOME/.kube/config
## 下载并安装flannel网络插件: https://github.com/flannel-io/flannel/blob/master/Documentation/kube-flannel.yml
### 修改net-conf.json: "Network": "10.188.0.0/12" 即可(与kubeadm中networking.serviceSubnet一致)
$ kubectl apply -f kube-flannel.yml

## 添加master节点
### 证书私钥
$ kubeadm init phase upload-certs --upload-certs #重新生成certificate-key
$ kubeadm join 172.16.239.132:6443 --token [TOKEN] \
    --discovery-token-ca-cert-hash sha256:[证书HASH值] \
    --control-plane --certificate-key [证书私钥]

:证书HASH值
$ openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | openssl dgst -sha256 -hex | sed 's/^.* //'


:Token `24h expired`
$ kubeadm token create --print-join-command
```


```bash
## Node加入
$ kubeadm join 172.16.239.132:6443 --token [TOKEN] --discovery-token-ca-cert-hash sha256:[证书HASH值]
```